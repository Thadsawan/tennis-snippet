class TennisGame {
    constructor() {
        this.playerA = 0
        this.playerB = 0
        this.scoreList = [
            'Love',
            'Fifteen',
            'Thirty',
            'Fourty'
        ]
    }

    start() {

    }

    echoScore() {
        var scoreString = this.scoreList[this.playerA] + ' - ' + this.scoreList[this.playerB]

        if (this.playerA === 4 && this.playerA > this.playerB)
            return 'PlayerA wins the game'
        else if (this.playerB === 4 && this.playerB > this.playerA)
            return 'PlayerB wins the game'
        else
            return scoreString

        /*if (this.playerA === 1)
            return 'Fifteen - Love'
        else if (this.playerA === 2)
            return 'Thirty - Love'
        else if (this.playerA === 3)
            return 'Fourty - Love'
        else if (this.playerA === 4 && this.playerA > this.playerB)
            return 'PlayerA wins the game'
        else if (this.playerB === 1)
            return 'Love - Fifteen'
        else if (this.playerB === 2)
            return 'Love - Thirty'
        else if (this.playerB === 3)
            return 'Love - Fourty'
        else if (this.playerB === 4 && this.playerB > this.playerA)
            return 'PlayerB wins the game'
        else
            return 'Love - Love'
            */
    }
    
test('"Love - Love" when start the game', () => {
    var game = new TennisGame()
    game.start()

    expect(game.echoScore()).toBe('Love - Love')
})

test('"Fifteen - Love" when PlayerA get first score', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('Fifteen - Love')
})

test('"Thirty - Love" when PlayerA get double score', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('Thirty - Love')
})

test('"Fourty - Love" when PlayerA get tripple score', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()
    game.playerAGetScore()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('Fourty - Love')
})

test('"PlayerA wins the game" when PlayerA get 4 score before PlayerB', () => {
    var game = new TennisGame()
    game.start()
    game.playerAGetScore()
    game.playerAGetScore()
    game.playerAGetScore()
    game.playerAGetScore()

    expect(game.echoScore()).toBe('PlayerA wins the game')
})

test('"Love - Fifteen" when PlayerB get first score', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('Love - Fifteen')
})

test('"Love - Thirty" when PlayerB get double score', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('Love - Thirty')
})

test('"Love - Fourty" when PlayerB get tripple score', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()
    game.playerBGetScore()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('Love - Fourty')
})

test('"PlayerB wins the game" when PlayerB get 4 scores before PlayerA', () => {
    var game = new TennisGame()
    game.start()
    game.playerBGetScore()
    game.playerBGetScore()
    game.playerBGetScore()
    game.playerBGetScore()

    expect(game.echoScore()).toBe('PlayerB wins the game')
})
